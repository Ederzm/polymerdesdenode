#Imagen Base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copio archivos de build
ADD /build/default /app/build/default
ADD server.js /app
ADD package.json /app


#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comando de ejecucion
CMD ["npm","start"]
